var express = require('express');
var router = express.Router();

const path = require('path');
const configuration = require(path.join(__dirname, "../public/data.json"));
const ui = Object.keys(configuration.ui).map(key => ({
  name: [key, configuration.names[key]],
  elements: configuration.ui[key].map(symbol => [symbol, configuration.names[symbol]])
}));

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {title: 'Konfigurator', ui, configuration});
});

module.exports = router;
