// A, ¬A
// 'A', '-A'
export type PredicateLiteral = string;

export interface HornClause {
  // A ∧ ¬B ∧ C ⇒ ¬F
  // left: ['A', '-B', 'C']
  // right: '-F'
  left: PredicateLiteral[];
  right: PredicateLiteral;
}

// A ⇒ ⊤
export type HornFact = PredicateLiteral;

export interface KnowledgeBase {
  clauses: HornClause[];
  facts: HornFact[];
}

export interface HornGoalClause {
  // A ∧ B ∧ C ∧ D ⇒ ⊥
  literals: PredicateLiteral[];
}

export enum ConflictResult {
  noConflict,
  contradiction,
  noFacts
}

interface StackGoalEntry {
  state: PredicateLiteral[];
  ruleIndex?: number;
  currentIndex: number;
  flippedTarget?: boolean;
}

interface FastGoalAccessEntry {
  negated: boolean;
  value?: PredicateLiteral[];
}

interface FastGoalAccess {
  [key: string]: (FastGoalAccessEntry | FastGoalAccessEntry[]);
}


const splitLiteral = (literal: PredicateLiteral): [isFlipped: boolean, literal: PredicateLiteral] => {
  console.assert(literal.length > 0);
  return literal[0] === '-' ? [true, literal.substr(1)] : [false, literal];
};

export class HornClauseKnowledgeBase {
  private readonly fastAccess: FastGoalAccess = {};
  private stateStack: StackGoalEntry[] = [];
  private target: boolean = false;
  private lastResult: ConflictResult = ConflictResult.noConflict;
  private dejaVuStack: FastGoalAccessEntry[] = [];

  constructor(base: KnowledgeBase) {
    for (const clause of base.clauses) {
      const [negated, literal] = splitLiteral(clause.right);
      this.fastAccess[literal] = [...(this.fastAccess[literal] as FastGoalAccessEntry[] || []), {
        negated,
        value: clause.left
      }];
    }
    for (const fact of base.facts) {
      const [negated, literal] = splitLiteral(fact);
      this.fastAccess[literal] = { negated };
    }
  }

  checkIsConflicting(goal: HornGoalClause): ConflictResult {
    this.target = false;
    this.lastResult = ConflictResult.noConflict;
    this.stateStack = [{
      state: goal.literals,
      currentIndex: 0
    }];
    while (this.stateStack.length) {
      const entry = this.stateStack[this.stateStack.length - 1];
      if (entry.currentIndex >= entry.state.length) {
        this.dejaVuStack.pop();
        if (this.checkHornClauseEnding(this.stateStack.pop()!)) {
          return this.lastResult;
        }
        continue;
      }
      this.checkHornClauseLiteralConflicting(entry);
    }
    throw new Error('This should not happen!');
  }

  private checkHornClauseEnding(entry: StackGoalEntry): boolean {
    const currentEntry = this.stateStack[this.stateStack.length - 1];
    if (!currentEntry) {
      return true;
    }
    const [negated, literal] = splitLiteral(currentEntry.state[currentEntry.currentIndex - 1]);
    const previous = this.target;
    this.target = entry.flippedTarget ? !this.target : this.target;
    const clauses = this.fastAccess[literal];
    if (this.lastResult === ConflictResult.contradiction && !this.target) {
      if (!Array.isArray(clauses) || !this.pushRule(clauses, negated, currentEntry, entry.ruleIndex! + 1, this.target)) {
        this.fastAccess[literal] = { negated: true };
        currentEntry.currentIndex = currentEntry.state.length;
        return false;
      }
    }
    if (this.lastResult === ConflictResult.noConflict && this.target) {
      this.fastAccess[literal] = { negated: false };
      currentEntry.currentIndex = currentEntry.state.length;
      return false;
    }
    if (this.lastResult === ConflictResult.noFacts) {
      if (!Array.isArray(clauses)) {
        this.evaluateFact(clauses, negated, entry);
        return false;
      }
      this.pushRule(clauses, negated, currentEntry, entry.ruleIndex! + 1, this.target);
    } else if (Array.isArray(clauses) && entry.ruleIndex! + 1 >= clauses.length) {
      this.fastAccess[literal] = { negated: false };
    } else if (!Array.isArray(clauses)) {
      this.fastAccess[literal] = { negated: this.lastResult === ConflictResult.contradiction ? !previous : previous };
    }
    return false;
  }

  private checkHornClauseLiteralConflicting(entry: StackGoalEntry): void {
    const [negated, literal] = splitLiteral(entry.state[entry.currentIndex++]);
    const clausesOrFact = this.fastAccess[literal];
    if (clausesOrFact === undefined) {
      this.lastResult = ConflictResult.noFacts;
      entry.currentIndex = entry.state.length;
      return;
    }
    if (!Array.isArray(clausesOrFact)) {
      this.evaluateFact(clausesOrFact, negated, entry);
      return;
    }
    this.pushRule(clausesOrFact, negated, entry);
  }

  private evaluateFact(fact: FastGoalAccessEntry, negated: boolean, entry: StackGoalEntry) {
    if (fact.negated !== negated) {
      this.lastResult = this.target ? ConflictResult.noConflict : ConflictResult.contradiction;
      entry.currentIndex = entry.state.length;
      return;
    }
    this.lastResult = this.target ? ConflictResult.contradiction : ConflictResult.noConflict;
  }

  private pushRule(clauses: FastGoalAccessEntry[], currentNegated: boolean, entry: StackGoalEntry, startRuleIndex = 0, forceNext = true): boolean {
    while (startRuleIndex < clauses.length && this.dejaVuStack.indexOf(clauses[startRuleIndex]) >= 0) {
      startRuleIndex++;
    }
    if (startRuleIndex >= clauses.length) {
      if (forceNext) {
        entry.currentIndex = entry.state.length;
        this.lastResult = ConflictResult.noFacts;
      }
      return false;
    }
    const clause = clauses[startRuleIndex];
    this.lastResult = ConflictResult.noConflict;
    const isFlipped = currentNegated !== clause.negated;
    if (isFlipped) {
      this.target = !this.target;
    }
    this.dejaVuStack.push(clause);
    this.stateStack.push({
      state: clause.value!,
      currentIndex: 0,
      flippedTarget: isFlipped,
      ruleIndex: startRuleIndex
    });
    return true;
  }
}
