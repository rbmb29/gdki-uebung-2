import { ConflictResult, HornClauseKnowledgeBase } from './hornClauseCalculations';

let frontendData: any = (window as any)['frontendData'];

export const checkAll = (): void => {
  const literals = Object.keys(frontendData.names)
      .map((elem: string) => {
        return (document.getElementById('literal-' + elem) as HTMLInputElement)?.checked ? elem : null;
      }).filter(e => e);
  const mainContainers = Object.keys(frontendData.names)
      .map((elem: string) => [elem, document.getElementById(elem + '-choose'), document.getElementById(elem + '-okay')])
      .filter(e => e[0] && e[1]);
  const base = new HornClauseKnowledgeBase({
    clauses: frontendData.rules,
    facts: [...frontendData.facts, ...literals]
  });
  for (const container of mainContainers) {
    debugger;
    const result = base.checkIsConflicting({ literals: [container[0] as string] });
    console.log(result, ConflictResult[result], container[0]);
    if (result === ConflictResult.noConflict) {
      (container[1] as HTMLElement).classList.toggle('visually-hidden', true);
      (container[2] as HTMLElement).classList.toggle('visually-hidden', false);
    } else {
      (container[1] as HTMLElement).classList.toggle('visually-hidden', false);
      (container[2] as HTMLElement).classList.toggle('visually-hidden', true);
    }
  }
  for (const key of Object.keys(frontendData.names)) {
    const dom = document.getElementById('literal-' + key) as HTMLInputElement;
    if (!dom) {
      continue;
    }
    let result;
    if (dom.checked) {
      const subLiterals = literals.filter(e => e !== key);
      const subBase = new HornClauseKnowledgeBase({
        clauses: frontendData.rules,
        facts: [...frontendData.facts, ...subLiterals]
      });
      result = subBase.checkIsConflicting({
        literals: ['-' + key]
      });
    } else {
      result = base.checkIsConflicting({
        literals: [key]
      });
    }
    dom.disabled = result === ConflictResult.contradiction;
  }
};

(window as any)['checkAll'] = checkAll;
