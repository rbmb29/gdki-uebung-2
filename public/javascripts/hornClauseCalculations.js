"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HornClauseKnowledgeBase = exports.ConflictResult = void 0;
var ConflictResult;
(function (ConflictResult) {
    ConflictResult[ConflictResult["noConflict"] = 0] = "noConflict";
    ConflictResult[ConflictResult["contradiction"] = 1] = "contradiction";
    ConflictResult[ConflictResult["noFacts"] = 2] = "noFacts";
})(ConflictResult = exports.ConflictResult || (exports.ConflictResult = {}));
var splitLiteral = function (literal) {
    console.assert(literal.length > 0);
    return literal[0] === '-' ? [true, literal.substr(1)] : [false, literal];
};
var HornClauseKnowledgeBase = /** @class */ (function () {
    function HornClauseKnowledgeBase(base) {
        this.fastAccess = {};
        this.stateStack = [];
        this.target = false;
        this.lastResult = ConflictResult.noConflict;
        this.dejaVuStack = [];
        for (var _i = 0, _a = base.clauses; _i < _a.length; _i++) {
            var clause = _a[_i];
            var _b = splitLiteral(clause.right), negated = _b[0], literal = _b[1];
            this.fastAccess[literal] = __spreadArrays((this.fastAccess[literal] || []), [{
                    negated: negated,
                    value: clause.left
                }]);
        }
        for (var _c = 0, _d = base.facts; _c < _d.length; _c++) {
            var fact = _d[_c];
            var _e = splitLiteral(fact), negated = _e[0], literal = _e[1];
            this.fastAccess[literal] = { negated: negated };
        }
    }
    HornClauseKnowledgeBase.prototype.checkIsConflicting = function (goal) {
        this.target = false;
        this.lastResult = ConflictResult.noConflict;
        this.stateStack = [{
                state: goal.literals,
                currentIndex: 0
            }];
        while (this.stateStack.length) {
            var entry = this.stateStack[this.stateStack.length - 1];
            if (entry.currentIndex >= entry.state.length) {
                this.dejaVuStack.pop();
                if (this.checkHornClauseEnding(this.stateStack.pop())) {
                    return this.lastResult;
                }
                continue;
            }
            this.checkHornClauseLiteralConflicting(entry);
        }
        throw new Error('This should not happen!');
    };
    HornClauseKnowledgeBase.prototype.checkHornClauseEnding = function (entry) {
        var currentEntry = this.stateStack[this.stateStack.length - 1];
        if (!currentEntry) {
            return true;
        }
        var _a = splitLiteral(currentEntry.state[currentEntry.currentIndex - 1]), negated = _a[0], literal = _a[1];
        var previous = this.target;
        this.target = entry.flippedTarget ? !this.target : this.target;
        var clauses = this.fastAccess[literal];
        if (this.lastResult === ConflictResult.contradiction && !this.target) {
            if (!Array.isArray(clauses) || !this.pushRule(clauses, negated, currentEntry, entry.ruleIndex + 1, this.target)) {
                this.fastAccess[literal] = { negated: true };
                currentEntry.currentIndex = currentEntry.state.length;
                return false;
            }
        }
        if (this.lastResult === ConflictResult.noConflict && this.target) {
            this.fastAccess[literal] = { negated: false };
            currentEntry.currentIndex = currentEntry.state.length;
            return false;
        }
        if (this.lastResult === ConflictResult.noFacts) {
            if (!Array.isArray(clauses)) {
                this.evaluateFact(clauses, negated, entry);
                return false;
            }
            this.pushRule(clauses, negated, currentEntry, entry.ruleIndex + 1, this.target);
        }
        else if (Array.isArray(clauses) && entry.ruleIndex + 1 >= clauses.length) {
            this.fastAccess[literal] = { negated: false };
        }
        else if (!Array.isArray(clauses)) {
            this.fastAccess[literal] = { negated: this.lastResult === ConflictResult.contradiction ? !previous : previous };
        }
        return false;
    };
    HornClauseKnowledgeBase.prototype.checkHornClauseLiteralConflicting = function (entry) {
        var _a = splitLiteral(entry.state[entry.currentIndex++]), negated = _a[0], literal = _a[1];
        var clausesOrFact = this.fastAccess[literal];
        if (clausesOrFact === undefined) {
            this.lastResult = ConflictResult.noFacts;
            entry.currentIndex = entry.state.length;
            return;
        }
        if (!Array.isArray(clausesOrFact)) {
            this.evaluateFact(clausesOrFact, negated, entry);
            return;
        }
        this.pushRule(clausesOrFact, negated, entry);
    };
    HornClauseKnowledgeBase.prototype.evaluateFact = function (fact, negated, entry) {
        if (fact.negated !== negated) {
            this.lastResult = this.target ? ConflictResult.noConflict : ConflictResult.contradiction;
            entry.currentIndex = entry.state.length;
            return;
        }
        this.lastResult = this.target ? ConflictResult.contradiction : ConflictResult.noConflict;
    };
    HornClauseKnowledgeBase.prototype.pushRule = function (clauses, currentNegated, entry, startRuleIndex, forceNext) {
        if (startRuleIndex === void 0) { startRuleIndex = 0; }
        if (forceNext === void 0) { forceNext = true; }
        while (startRuleIndex < clauses.length && this.dejaVuStack.indexOf(clauses[startRuleIndex]) >= 0) {
            startRuleIndex++;
        }
        if (startRuleIndex >= clauses.length) {
            if (forceNext) {
                entry.currentIndex = entry.state.length;
                this.lastResult = ConflictResult.noFacts;
            }
            return false;
        }
        var clause = clauses[startRuleIndex];
        this.lastResult = ConflictResult.noConflict;
        var isFlipped = currentNegated !== clause.negated;
        if (isFlipped) {
            this.target = !this.target;
        }
        this.dejaVuStack.push(clause);
        this.stateStack.push({
            state: clause.value,
            currentIndex: 0,
            flippedTarget: isFlipped,
            ruleIndex: startRuleIndex
        });
        return true;
    };
    return HornClauseKnowledgeBase;
}());
exports.HornClauseKnowledgeBase = HornClauseKnowledgeBase;
