"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkAll = void 0;
var hornClauseCalculations_1 = require("./hornClauseCalculations");
var frontendData = window['frontendData'];
var checkAll = function () {
    var literals = Object.keys(frontendData.names)
        .map(function (elem) {
        var _a;
        return ((_a = document.getElementById('literal-' + elem)) === null || _a === void 0 ? void 0 : _a.checked) ? elem : null;
    }).filter(function (e) { return e; });
    var mainContainers = Object.keys(frontendData.names)
        .map(function (elem) { return [elem, document.getElementById(elem + '-choose'), document.getElementById(elem + '-okay')]; })
        .filter(function (e) { return e[0] && e[1]; });
    var base = new hornClauseCalculations_1.HornClauseKnowledgeBase({
        clauses: frontendData.rules,
        facts: __spreadArrays(frontendData.facts, literals)
    });
    for (var _i = 0, mainContainers_1 = mainContainers; _i < mainContainers_1.length; _i++) {
        var container = mainContainers_1[_i];
        debugger;
        var result = base.checkIsConflicting({ literals: [container[0]] });
        console.log(result, hornClauseCalculations_1.ConflictResult[result], container[0]);
        if (result === hornClauseCalculations_1.ConflictResult.noConflict) {
            container[1].classList.toggle('visually-hidden', true);
            container[2].classList.toggle('visually-hidden', false);
        }
        else {
            container[1].classList.toggle('visually-hidden', false);
            container[2].classList.toggle('visually-hidden', true);
        }
    }
    var _loop_1 = function (key) {
        var dom = document.getElementById('literal-' + key);
        if (!dom) {
            return "continue";
        }
        var result = void 0;
        if (dom.checked) {
            var subLiterals = literals.filter(function (e) { return e !== key; });
            var subBase = new hornClauseCalculations_1.HornClauseKnowledgeBase({
                clauses: frontendData.rules,
                facts: __spreadArrays(frontendData.facts, subLiterals)
            });
            result = subBase.checkIsConflicting({
                literals: ['-' + key]
            });
        }
        else {
            result = base.checkIsConflicting({
                literals: [key]
            });
        }
        dom.disabled = result === hornClauseCalculations_1.ConflictResult.contradiction;
    };
    for (var _a = 0, _b = Object.keys(frontendData.names); _a < _b.length; _a++) {
        var key = _b[_a];
        _loop_1(key);
    }
};
exports.checkAll = checkAll;
window['checkAll'] = exports.checkAll;
