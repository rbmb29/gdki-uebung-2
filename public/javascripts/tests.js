"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var hornClauseCalculations_1 = require("./hornClauseCalculations");
var test0 = {
    clauses: [
        {
            left: ['A'],
            right: 'G'
        },
        {
            left: ['B'],
            right: 'G'
        },
        {
            left: ['C'],
            right: 'G'
        },
        {
            left: ['-A', '-C'],
            right: 'B'
        },
        {
            left: ['-A', '-B'],
            right: 'C'
        },
        {
            left: ['-B', '-C'],
            right: 'A'
        }
    ],
    facts: ['B']
};
var v = new hornClauseCalculations_1.HornClauseKnowledgeBase(test0).checkIsConflicting({
    literals: ['E']
});
console.log(hornClauseCalculations_1.ConflictResult[v]);
var test1 = {
    clauses: [
        {
            left: ['K'],
            right: 'W'
        },
        {
            left: ['W'],
            right: 'K'
        },
        {
            left: ['K'],
            right: 'S'
        },
        {
            left: ['S'],
            right: 'K'
        },
        {
            left: ['-K'],
            right: '-S'
        },
        {
            left: ['-S'],
            right: '-K'
        }
    ],
    facts: ['S']
};
var test1Goal = {
    literals: ['W']
};
//console.assert(new HornClauseKnowledgeBase(test1).checkIsConflicting(test1Goal) === ConflictResult.noConflict);
//console.log('Test 1 passed');
var test2 = {
    clauses: [
        {
            left: ['-S'],
            right: 'N'
        },
        {
            left: ['-N'],
            right: 'S'
        },
        {
            left: ['-N'],
            right: 'P'
        },
        {
            left: ['-P'],
            right: 'N'
        },
        {
            left: ['N'],
            right: 'P'
        },
        {
            left: ['-P'],
            right: '-N'
        }
    ],
    facts: ['-S']
};
var test2Base = new hornClauseCalculations_1.HornClauseKnowledgeBase(test2);
//console.assert(test2Base.checkIsConflicting({ literals: ['N'] }) === ConflictResult.noConflict);
//console.assert(test2Base.checkIsConflicting({ literals: ['-N'] }) === ConflictResult.contradiction);
//console.assert(test2Base.checkIsConflicting({ literals: ['P'] }) === ConflictResult.contradiction);
//console.assert(test2Base.checkIsConflicting({ literals: ['-P'] }) === ConflictResult.noConflict);
//console.log('Test 2 passed');
var test3 = {
    "clauses": [
        {
            "left": [
                "-T0-0",
                "-T0-1",
                "-T0-2"
            ],
            "right": "-T0"
        },
        {
            "left": [
                "T0-A",
                "-T0-B",
                "-T0-C"
            ],
            "right": "T0-0"
        },
        {
            "left": [
                "-T0-A",
                "T0-B",
                "-T0-C"
            ],
            "right": "T0-1"
        },
        {
            "left": [
                "-T0-A",
                "-T0-B",
                "T0-C"
            ],
            "right": "T0-2"
        },
        {
            "left": [
                "T0"
            ],
            "right": "T5"
        }
    ],
    facts: ['T0-A', '-T0-B', '-T0-C']
};
var test3Goal = {
    literals: ['T0']
};
var base = new hornClauseCalculations_1.HornClauseKnowledgeBase(test3);
//console.log(ConflictResult[base.checkIsConflicting(test3Goal)]);
