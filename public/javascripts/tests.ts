import { ConflictResult, HornClauseKnowledgeBase, HornGoalClause, KnowledgeBase } from './hornClauseCalculations';

const test0: KnowledgeBase = {
  clauses: [
    {
      left: ['A'],
      right: 'G'
    },
    {
      left: ['B'],
      right: 'G'
    },
    {
      left: ['C'],
      right: 'G'
    },
    {
      left: ['-A', '-C'],
      right: 'B'
    },
    {
      left: ['-A', '-B'],
      right: 'C'
    },
    {
      left: ['-B', '-C'],
      right: 'A'
    }
  ],
  facts: ['B']
};

const v = new HornClauseKnowledgeBase(test0).checkIsConflicting({
  literals: ['E']
})

console.log(ConflictResult[v]);

const test1: KnowledgeBase = {
  clauses: [
    {
      left: ['K'],
      right: 'W'
    },
    {
      left: ['W'],
      right: 'K'
    },
    {
      left: ['K'],
      right: 'S'
    },
    {
      left: ['S'],
      right: 'K'
    },
    {
      left: ['-K'],
      right: '-S'
    },
    {
      left: ['-S'],
      right: '-K'
    }
  ],
  facts: ['S']
};

const test1Goal: HornGoalClause = {
  literals: ['W']
};

//console.assert(new HornClauseKnowledgeBase(test1).checkIsConflicting(test1Goal) === ConflictResult.noConflict);
//console.log('Test 1 passed');


const test2: KnowledgeBase = {
  clauses: [
    {
      left: ['-S'],
      right: 'N'
    },
    {
      left: ['-N'],
      right: 'S'
    },
    {
      left: ['-N'],
      right: 'P'
    },
    {
      left: ['-P'],
      right: 'N'
    },
    {
      left: ['N'],
      right: 'P'
    },
    {
      left: ['-P'],
      right: '-N'
    }
  ],
  facts: ['-S']
};


const test2Base = new HornClauseKnowledgeBase(test2);
//console.assert(test2Base.checkIsConflicting({ literals: ['N'] }) === ConflictResult.noConflict);
//console.assert(test2Base.checkIsConflicting({ literals: ['-N'] }) === ConflictResult.contradiction);
//console.assert(test2Base.checkIsConflicting({ literals: ['P'] }) === ConflictResult.contradiction);
//console.assert(test2Base.checkIsConflicting({ literals: ['-P'] }) === ConflictResult.noConflict);
//console.log('Test 2 passed');


const test3: KnowledgeBase = {
  "clauses": [
    {
      "left": [
        "-T0-0",
        "-T0-1",
        "-T0-2"
      ],
      "right": "-T0"
    },
    {
      "left": [
        "T0-A",
        "-T0-B",
        "-T0-C"
      ],
      "right": "T0-0"
    },
    {
      "left": [
        "-T0-A",
        "T0-B",
        "-T0-C"
      ],
      "right": "T0-1"
    },
    {
      "left": [
        "-T0-A",
        "-T0-B",
        "T0-C"
      ],
      "right": "T0-2"
    },
    {
      "left": [
        "T0"
      ],
      "right": "T5"
    }
  ],
  facts: ['T0-A', '-T0-B', '-T0-C']
};
const test3Goal: HornGoalClause = {
  literals: ['T0']
};

const base = new HornClauseKnowledgeBase(test3);
//console.log(ConflictResult[base.checkIsConflicting(test3Goal)]);
